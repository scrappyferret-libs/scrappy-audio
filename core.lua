--- Required libraries.
local Group = require( ( _G.scrappyDir or "" ) .. "audio.group" )
local Playlist = require( ( _G.scrappyDir or "" ) .. "audio.playlist" )

-- Localised functions.
local loadSound = audio.loadSound
local loadStream = audio.loadStream
local dispose = audio.dispose
local stop = audio.stop
local play = audio.play
local fadeOut = audio.fadeOut
local isChannelActive = audio.isChannelActive
local reserveChannels = audio.reserveChannels
local getVolume = audio.getVolume
local setVolume = audio.setVolume
local floor = math.floor
local performWithDelay = timer.performWithDelay

-- Localised values
local totalChannels = audio.totalChannels
local isSwitch = ( system.getInfo( "platform" ) == "nx64" and system.getInfo( "environment" ) == "device" )

--- Class creation.
local library = {}

-- Static values.
library.Type = {}
library.Type.SFX = "sfx"
library.Type.Music = "music"
library.Type.Voice = "voice"
library.Type.Background = "background"

--- Initialises this Scrappy library.
-- @param params The params for the initialisation.
function library:init( params )

	-- Store off the params
	self._params = params or {}

	-- Table for registered sound definitions
	self._sounds = {}

	-- Table for loaded sound handles
	self._handles = {}

	-- Table for custom volumes for individual handles
	self._handleVolumes = {}

	-- Table for registered sound groups
	self._groups = {}

	-- Table for registered playlists
	self._playlists = {}

	-- Table to store what sounds are playing on what channels
	self._activeChannels = {}

	-- Table to store current volumes
	self._volumes = {}

	-- Set up channel counts
	self._channelCounts = self._params.channelCounts or {}
	self._channelCounts.music = self._channelCounts.music or 5
	self._channelCounts.voice = self._channelCounts.voice or 5
	self._channelCounts.background = self._channelCounts.background or 5
	self._channelCounts.sfx = self._channelCounts.sfx or ( totalChannels - ( self._channelCounts.music + self._channelCounts.voice + self._channelCounts.background ) )

	-- Set up a list of channel types
	self._channels = {}

	-- Loop through all types and set up an empty table
	for k, v in pairs( library.Type ) do
		self._channels[ v ] = {}
	end

	-- Loop through all channels
	for i = 1, totalChannels, 1 do

		-- Mark the channel as inactive
		self._activeChannels[ i ] = false

		-- Is this index for music channels?
		if i <= self._channelCounts.music then

			-- Then add it to the list
			self._channels.music[ #self._channels.music + 1 ] = i

		-- Or is this for voice channels?
		elseif i <= self._channelCounts.music + self._channelCounts.voice then

			-- Then add it to the list
			self._channels.voice[ #self._channels.voice + 1 ] = i

		-- Or is this for background channels?
		elseif i <= self._channelCounts.music + self._channelCounts.voice + self._channelCounts.background then

			-- Then add it to the list
			self._channels.background[ #self._channels.background + 1 ] = i

		-- Otherwise it must be for sfx
		else

			-- Then add it to the list
			self._channels.sfx[ #self._channels.sfx + 1 ] = i

		end

	end

	-- Reserve the required channels
	reserveChannels( self._channelCounts.music + self._channelCounts.voice + self._channelCounts.background + self._channelCounts.sfx )

	-- Register system event listener.
	Runtime:addEventListener( "system", self )

end

--- Registers a sound group.
-- @param name The name of the group.
-- @param sounds A list of registered sounds to add.
-- @return The name of the group.
function library:registerGroup( name, sounds )
	self._groups[ name ] = Group:new( name, sounds )
	return name
end

--- Gets a sound group.
-- @param name The name of the group.
-- @return The group.
function library:getGroup( name )
	return self._groups[ name ]
end

--- Loads all sounds in a group.
-- @param name The name of the group.
function library:loadGroup( name )

	-- Get the group
	local group = self:getGroup( name )

	-- Do we have a group?
	if group then

		-- Then load it
		group:load()

	end

end

--- Unloads all sounds in a group.
-- @param name The name of the group.
function library:unloadGroup( name )

	-- Get the group
	local group = self:getGroup( name )

	-- Do we have a group?
	if group then

		-- Then unload it
		group:unload()

	end

end

--- Registers a playlist.
-- @param name The name of the playlist.
-- @param sounds A list of registered sounds to add.
-- @return The name of the playlist.
function library:registerPlaylist( name, sounds )
	self._playlists[ name ] = Playlist.new( name, sounds )
	return self._playlists[ name ]
end

--- Gets a playlist.
-- @param name The name of the playlist.
-- @return The playlist.
function library:getPlaylist( name )
	return self._playlists[ name ]
end

--- Registers a sound.
-- @param name The name of the sound.
-- @param definition The definition of the sound - filename, baseDir, and type ( sfx, music, voice ).
-- @param notLazy Set to true if you want the sound to load right now. Optional, defaults to false for lazy loading.
function library:register( name, definition, notLazy )

	-- Store off the name
	definition.name = name

	-- Store the sound definition
	self._sounds[ name ] = definition

	-- Are we not being lazy today?
	if notLazy then

		-- Then load the sound now
		self:load( name )

	end

end

--- Gets the type of a registered sound.
-- @param name The name of the sound.
-- @return The type.
function library:getSoundType( name )

	-- Is this name actually a loaded sound handle?
	if type( name ) == "userdata" then

		-- Get the definition of this sound
		local definition = self:getDefinition( name )

		-- Do we have a definition?
		if definition then

			-- Then pull out its' name
			name = definition.name

		end


	end

	-- Do we have a name and a valid definition?
	if name and self._sounds[ name ] then

		-- Return the type
		return self._sounds[ name ].type

	end

end

--- Checks if a sound file has been registered.
-- @param path The path of the sound file.
-- @return True if it has, false otherwise.
function library:isFileRegistered( path )

	-- Loop through all registered sounds
	for k, v in pairs( self._sounds ) do

		-- Check if the sound's filename matches this path
		if v.filename == path then

			-- And return true if it does
			return true

		end

	end

end

--- Loads a sound.
-- @param name The name of the sound.
-- @return The loaded handle.
function library:load( name )

	-- Check we're not already loaded
	if not self:get( name ) then

		-- Get the definition
		local definition = self:getDefinition( name )

		-- Do we have one?
		if definition then

			-- Is this typeless or SFX?
			if not definition.type or definition.type == self.Type.SFX then

				definition.filename = definition.filename:gsub( "\\", "/" )
				
				-- Is this an opus? And we're not on a Switch?
				if definition.filename:find( ".opus" ) and not isSwitch then
					
					-- Try to load up an mp3 file instead
					self._handles[ name ] = loadSound( definition.filename:gsub( ".opus", ".mp3" ), definition.baseDir )
					
				end
				
				-- Is this an mp3? And we're on a Switch?
				if definition.filename:find( ".mp3" ) and isSwitch then
					
					-- Try to load up an opus file instead
					self._handles[ name ] = loadSound( definition.filename:gsub( ".mp3", ".opus" ), definition.baseDir )
					
				end
				
				-- If we haven't already loaded it, then load it now
				if not self._handles[ name ] then
					
					-- Then load it as a sound
					self._handles[ name ] = loadSound( definition.filename, definition.baseDir )
					
				end
				
			-- Otherwise it must be music or voice
			elseif definition.type == self.Type.Music or definition.type == self.Type.Voice or definition.type == self.Type.Background then
				
				
				-- Is this an opus? And we're not on a Switch?
				if definition.filename:find( ".opus" ) and not isSwitch then
					
					-- Try to load up an mp3 file instead
					self._handles[ name ] = loadStream( definition.filename:gsub( ".opus", ".mp3" ), definition.baseDir )
					
				end
				
				-- Is this an mp3? And we're on a Switch?
				if definition.filename:find( ".mp3" ) and isSwitch then
					
					-- Try to load up an opus file instead
					self._handles[ name ] = loadStream( definition.filename:gsub( ".mp3", ".opus" ), definition.baseDir )
					
				end
				
				-- If we haven't already loaded it, then load it now
				if not self._handles[ name ] then
					
					-- So load as a stream
					self._handles[ name ] = loadStream( definition.filename, definition.baseDir )
					
				end

			end

		end

	end

	-- Return the loaded handle
	return self:get( name )

end

--- Unloads a sound.
-- @param name The name of the sound.
function library:unload( name )

	-- Has the sound been loaded?
	if self:get( name ) then

		-- Then stop it
		self:stop( name )

		-- And dispose of it
		if self._handles[ name ] then
			dispose( self._handles[ name ] )
		end

		-- Then nil it out
		self._handles[ name ] = nil

	end

end

--- Plays a sound.
-- @param name The name of the sound, or an already loaded handle.
-- @param options The options for the playing, as per Solar2D docs ( barring channel as that's controlled by Scrappy Audio ).
-- @param force Should the sound be played even if it's already being played? Optional, defaults to false.
-- @return The channel it was played on, the played handle, and the sound source.
function library:play( name, options, force )

	-- Get the definition of this sound
	local definition = self:getDefinition( name )

	-- Pre-declare the source
	local source

	-- Do we have a definition?
	if definition and ( not self:isPlaying( name ) or force ) then

		-- Load the sound if need be
		local handle = self:load( definition.name )

		-- Set up the options table
		local options = options or {}

		-- Get a channel for this sound type
		local foundChannel = self:getFreeChannel( definition.type )
		options.channel = foundChannel

		-- Store off the original on complete callback
		options.cachedOnComplete = options.onComplete

		-- Override the onComplete callback with this one
		options.onComplete = function( event )

			-- Did we have a custom pitch?
			if options and options.pitch then

				-- Set it back
				al.Source( source, al.PITCH, 1 )

			end
			
			if event.completed then
			
				-- Channel no longer active so remove the sound's name
				self._activeChannels[ options.channel ] = false
				
			end
			
			-- Do we have a valid cached onComplete?
			if options.cachedOnComplete and type( options.cachedOnComplete ) == "function" then

				-- Call it
				options.cachedOnComplete( event )

			end

		end

		-- Get the definition
		local definition = self:getDefinition( name )

		-- Do we have one?
		if definition and options.channel then

			-- Pre-declare the channel
			local channel

			-- And finally play the sound
			channel, source = play( handle, options )

			-- Try to get a specific handle volume for this handle
			local handleVolume = self:getHandleVolume( definition.name )

			-- If we don't have one
			if not handleVolume then

				-- Then try again but with the handle directly
				handleVolume = self:getHandleVolume( handle )

			end

			-- Do we have one?
			if handleVolume then

				-- Get the volume for this sound type
				local typeVolume = self:getVolume( definition.type )

				-- Calculate the new volume based on the type volume and our named in volume
				options.volume = typeVolume * handleVolume

			end

			if channel > 0 then

				-- Mark the channel as active
				self._activeChannels[ channel ] = definition.name

				-- Was a volume passed in?
				if options.volume then

					if self:isMuted() then
						options.volume = 0
					end

					-- Then set the volume for the channel
					self:setVolume( options.volume, channel )

				-- Otherwise
				else

					-- Get the volume for this type
					local volume = self:getVolume( definition.type )

					if self:isMuted() then
						volume = 0
					end

					-- Then set the volume for the channel
					self:setVolume( volume, channel )

				end

			else

				-- Mark the channel as active
				self._activeChannels[ foundChannel ] = nil

				for i = 1, #( self._activeChannels or {} ) do
					if self._activeChannels[ i ] == definition.name then
						self._activeChannels[ i ] = nil
					end
				end

			end

			-- Do we have a custom pitch value?
			if options and options.pitch then

				-- Then set it
				al.Source( source, al.PITCH, options.pitch )

			end

			-- Return the channel, handle, and source
			return channel, handle, source

		end

	end

end

--- Stops a sound, across all channels it's playing on.
-- @param name The name of the sound. Or a channel number if you want to get really specific. Or all sounds if nothing is specified.
function library:stop( name, time )


	local resetVolume = function( channel, level )
		setVolume( level, { channel = channel } )
	end

	-- Do we have a name or channel?
	if name then

		-- Was it a name?
		if type( name ) == "string" then

			-- Loop through all active channels\
			for i = 1, #( self._activeChannels or {} ) do

				-- Is this sound playing on this channel?
				if self._activeChannels[ i ] == name then

					-- Then stop it
					self:stop( i, time )

				end

			end

		-- Or a channel?
		elseif type( name ) == "number" then

			if time then

				-- Get the current volume
				local volume = getVolume{ channel = name }

				-- Fades out the channel
				fadeOut( { channel = name, time = time } )

				-- Reset the volume
				performWithDelay( time, function() setVolume( volume, { channel = name } ) end, 1 )

			else
				-- Stop the channel
				stop( name )
			end

			-- And mark it as not active
			self._activeChannels[ name ] = false

		end

	else

		if time then

			-- Get the current volume
			local volume = getVolume{ channel = 0 }

			-- Fades out all channels
			fadeOut( { channel = 0, time = time } )

			-- Reset the volume
			performWithDelay( time, function() setVolume( volume, { channel = 0 } ) end, 1 )

		else
			-- Stop all channels
			stop()
		end

		-- And mark them as not active
		self._activeChannels = {}

	end

end

--- Pauses some audio.
-- @param name The name of the sound type, or the name of the handle, or the channel number, or the handle itself. Leave blank to pause everything.
function library:pause( name )

	-- Do we not have a name passed in?
	if not name then

		-- Loop through all playlists
		for k, v in pairs( self._playlists ) do

			-- Is it currently playing?
			if not v.isPaused() then

				-- Mark it as that it was playing
				v._wasPlaying = true

				-- And pause it
				v.pause()

			end

		end

		-- Mark us as paused
		self._paused = true

	end

	-- Call the pause function
	self:_callAudioFunction( name, audio.pause )

end

--- Checks if some audio is currently playing.
-- @param name The name of the audio.
-- @return True if it is, false otherwise.
function library:isPlaying( name )
	for i = 1, #self._activeChannels, 1 do
		if name == self._activeChannels[ i ] then
			return true
		end
	end
	return false
end

--- Resumes some audio.
-- @param name The name of the sound type, or the name of the handle, or the channel number, or the handle itself. Leave blank to resume everything.
function library:resume( name )

	-- Do we not have a name passed in?
	if not name then

		-- Loop through all playlists
		for k, v in pairs( self._playlists ) do

			-- Was it already playing?
			if v._wasPlaying then

				-- Resume it
				v.resume()

				-- And nil out the flag
				v._wasPlaying = nil

			end

		end

		-- Mark us as not paused
		self._paused = false

	end

	-- Call the resume function
	self:_callAudioFunction( name, audio.resume )

end

--- Checks if the system is paused or not.
-- @return True if it is, false otherwise.
function library:isPaused()
	return self._paused
end

--- Toggles the playing of the whole system.
function library:toggle()

	-- Are we paused?
	if self:isPaused() then

		-- Then resume us
		self:resume()

	-- Otherwise
	else

		-- Freeze!
		self:pause()

	end

end

--- Gets a sound handle.
-- @param name The name of the sound.
-- @return The loaded handle.
function library:get( name )
	return self._handles[ name ]
end

--- Gets a sound definition.
-- @param name The name of the sound, or an already loaded handle.
-- @return The registered definition.
function library:getDefinition( name )

	-- Is this name actually a loaded sound handle?
	if type( name ) == "userdata" then

		-- Then loop through the handles
		for k, v in pairs( self._handles ) do

			-- Until we find the right one
			if name == v then

				-- And then return the definition for this handle
				return self._sounds[ k ]

			end

		end

	-- Otherwise it's just a string
	else

		-- So return the definition
		return self._sounds[ name ]

	end

end

--- Gets a free channel.
-- @param type The type of channel.
-- @return The free channel, or nil if none are free.
function library:getFreeChannel( type )

	-- Loop through all channels of this type
	for i = 1, #self._channels[ type ], 1 do

		-- Check if this channel is not active
		if not isChannelActive( self._channels[ type ][ i ] ) then

			-- Then return the index
			return self._channels[ type ][ i ]

		end

	end

end

--- Sets the volume factor for a named handle.
-- @param name The name of the handle, or the handle object directly.
-- @param factor The volume factor to set. When this handle is played its volume will then be the master volume for its type multiplied by this factor value.
function library:setHandleVolume( name, factor )

	local name = self:getHandleName( name )

	if name then

		factor = factor or 0

		-- Store out the factor
		self._handleVolumes[ name ] = factor

		-- Are we already playing?
		if self:isPlaying( name ) and factor then

			-- Get the definition
			local definition = self:getDefinition( name )

			-- Get the volume for this sound type
			local typeVolume = self:getVolume( definition.type )

			-- Calculate the new volume based on the type volume and our named in volume
			local newVolume = typeVolume * factor

			-- Get the chanel for this sound
			local channel = self:getChannel( name )

			-- Do we have a channel?
			if channel and channel > 0 then

				if self:isMuted() then
					newVolume = 0
				end

				-- Set the new volume mid playback
				self:setVolume( newVolume, channel )

			end

		end

	end

end

--- Gets the volume factor for a named handle.
-- @param name The name of the handle, or the handle object directly.
-- @return The volume factor.
function library:getHandleVolume( name )
	return self._handleVolumes[ name ]
end


function library:getHandleName( handle )

	-- Loop through the handles
	for k, v in pairs( self._handles ) do
		if v == handle then
			return k
		end
	end

	return handle

end

--- Sets the volume of a channel.
-- @param type The type of audio, or the specific channel number. Optional, leave out if you want to set the master volume.
function library:setVolume( level, audioType )


	-- Do we have a type?
	if audioType then

		if self._volumes[ audioType ] ~= level or level == 0 then

			-- Store out the volume
			self._volumes[ audioType ] = level

			if type( audioType ) == "number" then

				-- And set the volume
				setVolume( level, { channel = audioType } )

			else

				-- Then loop through each channel of this type
				for i = 1, #self._channels[ audioType ], 1 do

					-- Is there a handle on this channel?
					if self._activeChannels[ self._channels[ audioType ][ i ] ] then

						local current = self:_round( getVolume( { channel = self._channels[ audioType ][ i ] } ), 2 )

						if current ~= level then

							-- Then set the volume
							setVolume( level, { channel = self._channels[ audioType ][ i ] } )

						end

					end

				end

			end

		end

	-- Otherwise
	else

		-- Just set the master volume
		setVolume( level )

	end

end

--- Gets the volume of a channel.
-- @param type The type of channel. Optional, leave out if you want to get the master volume.
-- @return The volume level.
function library:getVolume( type )

	-- Do we have a type?
	if type then

		-- Return the volume for this type
		return self._volumes[ type ]

	-- Otherwise
	else

		-- Just get the master volume
		return getVolume()

	end

end

--- Mutes the system.
function library:mute()

	-- Store off the current master volume
	self._volumeAtMute = self:getVolume()

	-- Set the master volume to 0
	self:setVolume( 0 )

end

--- Unmutes the system.
function library:unmute()

	-- Do we have a stored off volume?
	if self._volumeAtMute then

		-- Then set the master volume to it
		self:setVolume( self._volumeAtMute )

	end

	-- And nil out the store volume
	self._volumeAtMute = nil

end

--- Checks if the system is muted or not.
-- @return True if it is, false otherwise.
function library:isMuted()

	-- If we have a stored out volume then we must be muted
	return self._volumeAtMute ~= nil

end

--- Toggles mute on and off for the system.
function library:toggleMute()

	-- Are we muted?
	if self:isMuted() then

		-- Then unmute us
		self:unmute()

	-- Otherwise
	else

		-- Mute us
		self:mute()

	end

end

--- Gets the channel a sound handle is playing on.
-- @param name The name of the handle.
-- @return The channel number, or nil if not found.
function library:getChannel( name )

	-- Loop through the active channels
	for k, v in pairs( self._activeChannels ) do

		-- Does this match the name?
		if v == name then

			-- Then return the index
			return k

		end

	end

end

--- Calls a function on a sound.
-- @param name The name of the handle, or the handle, or the channel, or the sound type.
-- @param f The function to call.
function library:_callAudioFunction( name, f )

	-- Do we have a name?
	if name then

		-- Is this name actually a loaded sound handle?
		if type( name ) == "userdata" then

			-- Get the definition from this handle
			local definition = self:getDefinition( name )

			-- Do we have one?
			if definition then

				-- Then store off the name
				name = definition.name

			else

				-- Otherwise just nil out the name
				name = nil

			end

		end

		-- Do we still have a name?
		if name then

			-- Is the name a string?
			if type( name ) == "string" then

				-- Is the name one of our audio types?
				if self._channels[ name ] then

					-- Then loop through all the channels for this type
					for i = 1, #self._channels[ name ], 1 do

						-- Then run the function
						f( self._channels[ name ][ i ] )

					end

				-- Otherwise it must be the name of a sound
				else

					-- Get the channel
					local channel = self:getChannel( name )

					-- Do we have a channel number?
					if channel then

						-- Then run the function
						f( channel )

					end

				end


			-- Is the name a number? i.e. a channel
		elseif type( name ) == "number" then

				-- Then run the function
				f( name )

			end

		end

	else

		-- Just call the function
		f()

	end

end

--- Rounds a number.
-- @param number The number to round.
-- @param idp The number of decimal places to round to. Optional, defaults to 0.
-- @return The rounded number.
function library:_round( number, idp )
	local mult = 10 ^ ( idp or 0 )
	return floor( number * mult + 0.5 ) / mult
end

--- Event handler for 'system' events.
-- @param event The event table.
function library:system( event )
	if event.type == "applicationExit" then
		audio.stop()
		for k, v in pairs( self._handles ) do
			audio.dispose( v )
		end
	end
end

-- If we don't have a global Scrappy object i.e. this is the first Scrappy plugin to be included
if not Scrappy then

	-- Create one
	Scrappy = {}

end

-- If we don't have a Scrappy Audio library
if not Scrappy.Audio then

	-- Then store the library out
	Scrappy.Audio = library

end

-- Return the new library
return library
