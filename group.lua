--- Required libraries.

-- Localised functions.

-- Localised values.

--- Class creation.
local Group = {}

--- Initiates a new Group object.
-- @param name The name of the new Group.
-- @return The new Group.
function Group.new( name, sounds )

	-- Create ourselves
	local self = {}

	-- Set the name of this Group
	self._name = name

	-- Store off the list of sounds
	self._sounds = sounds

	--- Loads all sounds in the group.
	function self.load()

		-- Mark us as loaded
		self._isLoaded = true

		-- Loop through the sounds
		for i = 1, #( self._sounds or {} ), 1 do

			-- And load them
			Scrappy.Audio:load( self._sounds[ i ] )

		end

	end

	--- Unloads all sounds in the group.
	function self.unload()

		-- Mark us as not loaded
		self._isLoaded = false

		-- Loop through the sounds
		for i = 1, #( self._sounds or {} ), 1 do

			-- And unload them
			Scrappy.Audio:unload( self._sounds[ i ] )

		end

	end

	--- Checks if we're loaded or not.
	-- @return True if we are, false otherwise.
	function self.isLoaded()
		return self._isLoaded
	end

	--- Destroys this Group.
	function self.destroy()

		-- Unload us
		self.unload()

	end

	-- Return the new object
	return self

end

-- Return the class
return Group
