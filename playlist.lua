--- Required libraries.

-- Localised functions.
local cancel = transition.cancel
local to = transition.to
local random = math.random
local shuffle = function( t )

	for i = #t, 2, -1 do

	local j = Scrappy.Random and Scrappy.Random:inRange( i ) or random( i )

		t[ i ], t[ j ] = t[ j ], t[ i ]

	end

	return t

end

-- Localised values.

--- Class creation.
local Playlist = {}

--- Initiates a new Playlist object.
-- @param name The name of the new Playlist.
-- @return The new Playlist.
function Playlist.new( name, sounds )

	-- Create ourselves
	local self = {}

	-- Set the name of this Playlist
	self._name = name

	-- Store off the list of sounds
	self._sounds = sounds

	-- The current track index set to nil
	self._current = nil

	-- Table to store what channels we're using
	self._channels = {}

	-- Flag for whether we're paused or not
	self._paused = true

	-- Flag for whether we're playing or not
	self._playing = false

	-- Default volume
	self._volume = 1

	-- Add an enterFrame listener
	Runtime:addEventListener( "enterFrame", self )

	--- Starts playing this playlist.
	function self.play( params )

		-- Cancel any existing volume changes when we start playing
		self.cancelVolumeChange()

		-- Get the passed in params
		local params = params or {}

		-- Mark us as playing
		self._playing = true

		-- Set the volume if we've been provided one
		self.setVolume( params.volume or self.getVolume() or 1 )

		-- Do we not already have a current index?
	  	if not self._current then

			-- Then set it to a random one if required, or just the first
		  	self._current = self.isRandomModeEnabled() and self.random() or 1

	  	end

		-- onComplete handler for playing
	  	function self.onComplete( event )

			-- Was the sound playing completed? i.e. we didn't tell the track to stop playing
			if event.completed then

				if params.onComplete and type( params.onComplete )  == "function" then
					params.onComplete()
				end

				-- Are we in simultaneous mode?
				if self.isSimultaneousModeEnabled() then

					-- Get the type of this sound
					local type = Scrappy.Audio:getSoundType( event.handle )

					-- Then just start this one back again
					local channel, handle = Scrappy.Audio:play( event.handle, { onComplete = self.onComplete, volume = self.isMuted() and 0 or ( self.getVolume() * Scrappy.Audio:getVolume( type ) ) } )

					-- Do we have a valid handle?
					if handle then

						-- Then store off the channel number
						self._channels[ handle ] = channel

					end

				-- Otherwise
				else

					-- Play the next track
					self.next()

				end

			end

		end

		-- Are we in simultaneous mode?
		if self.isSimultaneousModeEnabled() then

			-- Then loop through each track
			for i = 1, #( self._sounds or {} ), 1 do

				-- Get the type of this sound
				local type = Scrappy.Audio:getSoundType( self._sounds[ i ] )

				-- And play it
				local channel, handle = Scrappy.Audio:play( self._sounds[ i ], { onComplete = self.onComplete, volume = self.isMuted() and 0 or ( self.getVolume() * Scrappy.Audio:getVolume( type ) ) } )

				-- Do we have a valid handle?
				if handle then

					-- Then store off the channel number
					self._channels[ handle ] = channel

				end

			end

		-- Otherwise
		else

			-- Get the type of this sound
			local type = Scrappy.Audio:getSoundType( self._sounds[ self._current ] )

			-- Just play the current track
			local channel, handle = Scrappy.Audio:play( self._sounds[ self._current ], { onComplete = self.onComplete, volume = self.isMuted() and 0 or ( self.getVolume() * Scrappy.Audio:getVolume( type ) ) } )

			-- Do we have a valid handle?
			if handle then

				-- Then store off the channel number
				self._channels[ handle ] = channel

			end

		end
	end

	--- Pauses this playlist.
	function self.pause()

		-- Is simultaneous mode enabled?
		if self.isSimultaneousModeEnabled() then

			-- Loop through all sounds
			for i = 1, #self._sounds, 1 do

				-- And pause them
				Scrappy.Audio:pause( self._sounds[ i ] )

			end

		-- Otherwise
		else

			-- Just pause the current one
	  		Scrappy.Audio:pause( self._sounds[ self._current ] )

		end

		-- And mark us as paused
		self._paused = true

		-- Mark us as not playing
		self._playing = false

	end

	--- Resumes this playlist.
	function self.resume()

		-- Is simultaneous mode enabled?
		if self.isSimultaneousModeEnabled() then

			-- Loop through all sounds
			for i = 1, #self._sounds, 1 do

				-- And resume them
				Scrappy.Audio:resume( self._sounds[ i ] )

				if not Scrappy.Audio:getChannel( self._sounds[ i ] ) then

					-- Resume this track
					local channel, handle = Scrappy.Audio:play( self._sounds[ i ], { onComplete = self.onComplete, volume = self.getVolume() } )

					-- And store off the channel number
					self._channels[ handle ] = channel

				end

			end

		-- Otherwise
		else

			-- Just resume the current one
	  		Scrappy.Audio:resume( self._sounds[ self._current ] )

		end

		-- And mark us as not paused
		self._paused = false

		-- Mark us as playing
		self._playing = true

	end

	--- Stops this playlist.
	function self.stop()

		-- Is simultaneous mode enabled?
		if self.isSimultaneousModeEnabled() then

			-- Loop through all sounds
			for i = 1, #self._sounds, 1 do

				-- And pause them
				if self._sounds[ i ] ~= nil then
					Scrappy.Audio:stop( self._sounds[ i ] )
				end

			end

		-- Otherwise
		else

			-- Just pause the current one
	  		if self._sounds[ self._current ] ~= nil then
				Scrappy.Audio:stop( self._sounds[ self._current ] )
			end

		end

		self._playing = false

	end

	--- Toggles the playing of this playlist.
	function self.toggle()

		-- Are we paused?
	  	if self.isPaused() then

			-- Then resume us
			self.resume()

		-- Otherwise
		else

			-- Freeze!
			self.pause()

		end

	end

	--- Checks if this playlist is paused or not.
	-- @return True if it is, false otherwise.
	function self.isPaused()
		return self._paused
	end

	--- Checks if this playlist is playing or not.
	-- @return True if it is, false otherwise.
	function self.isPlaying()
		return self._playing
	end

	--- Mutes the playlist.
	function self.mute()

		-- Store off the current volume
		self._volumeAtMute = self:getVolume()

		-- Set the volume to 0
		self.setVolume( 0 )

	end

	--- Unmutes the playlist.
	function self.unmute()

		-- Do we have a stored off volume?
		if self._volumeAtMute then

			-- Then set the volume to it
			self.setVolume( self._volumeAtMute )

		end

		-- And nil out the store volume
		self._volumeAtMute = nil

	end

	--- Checks if the playlist is muted or not.
	-- @return True if it is, false otherwise.
	function self.isMuted()

		-- If we have a stored out volume then we must be muted
		return self._volumeAtMute ~= nil

	end

	--- Toggles mute on and off for the playlist.
	function self.toggleMute()

		-- Are we muted?
		if self.isMuted() then

			-- Then unmute us
			self.unmute()

		-- Otherwise
		else

			-- Mute us
			self.mute()

		end

	end

	--- Plays the next track on this playlist.
	function self.next()

		-- Stop the current track
	  	self.stop()

		-- Are we in random mode?
	  	if self.isRandomModeEnabled() then

			-- Then get a random track index
		  	self._current = self.random()

	  	else

			-- Otherwise increment the track index
		  	self.incrementTrackIndex()

	  	end

		-- And play it
	  	self.play()

	end

	--- Plays the previous track on this playlist.
	function self.previous()

		-- Stop the current track
	  	self.stop()

		-- Are we in random mode?
	  	if self.isRandomModeEnabled() then

			-- Then get a random track index
		  	self._current = self.random()

	  	else

			-- Otherwise increment the track index
		  	self.incrementTrackIndex( - 1 )

	  	end

		-- And play it
	  	self.play()

	end

	--- Increments the current track index.
	-- @param count The number to increment by. Optional, defaults to 1.
	function self.incrementTrackIndex( count )

		-- Increment the track index by the passed in count or 1
		self._current = self._current + ( count or 1 )

		-- Now clamp it
		if self._current > #self._sounds then
			self._current = 1
		elseif self._current < 1 then
			self._current = #self._sounds
		end

	end

	--- Sets the current track index.
	-- @param index The track to play.
	function self.setTrackIndex( index )

		-- Set the index
		self._current = index or self._current

	end

	--- Gets the index of the current track.
	-- @return The track index.
	function self.getTrackIndex()
		return self._current
	end

	--- Shuffles the track listing.
	function self.shuffle()
	  	self._sounds = shuffle( self._sounds )
	end

	--- Enables random playback mode.
	function self.enableRandomMode()
	  	self._randomModeEnabled = true
	end

	--- Disables random playback mode.
	function self.disableRandomMode()
	  	self._randomModeEnabled = false
	end

	--- Checks if random playback mode is enabled.
	-- @return True if it is, false otherwise.
	function self.isRandomModeEnabled()
	  	return self._randomModeEnabled
	end

	--- Enables simultaneous playback mode.
	function self.enableSimultaneousMode()
	  	self._simultaneousModeEnabled = true
	end

	--- Disables simultaneous playback mode.
	function self.disableSimultaneousMode()
	  	self._simultaneousModeEnabled = false
	end

	--- Checks if simultaneous playback mode is enabled.
	-- @return True if it is, false otherwise.
	function self.isSimultaneousModeEnabled()
	  	return self._simultaneousModeEnabled
	end

	--- Sets the volume for the playlist.
	function self.setVolume( level )

		-- Is the new level different to what we are now?
		if level ~= self._volume then

			-- Store off the volume
			self._volume = level

			-- Loop through the handles
			for k, v in pairs( self._channels ) do

				-- And set the volume on the channel they're playing on
				Scrappy.Audio:setVolume( level, v )

			end

		end

	end

	--- Gets the volume of the playlist.
	-- @return The current volume level, or 1 if it hasn't been set.
	function self.getVolume()
		return self._volume
	end

	--- Changes the volume over time.
	-- @param level The new volume level.
	-- @param time The time for the change.
	-- @param callback The onComplete handler for the change. Optional.
	function self.changeVolume( level, time, callback )

		-- Cancel the current volume change transition
		self:cancelVolumeChange()

		-- onComplete handler for the volume transition
		local onComplete = function()

			-- Sanity check to make sure the volume is now as required
			self.setVolume( level )

			-- Are we now at 0?
			if level == 0 then

				-- Stop us
				--self.stop()

				-- Do we have an onComplete callback? If so, call it!
				if callback and type( callback ) == "function" then callback() end

			end

		end

		-- Get the type of this sound
		local type = Scrappy.Audio:getSoundType( self._sounds[ self._current ] )

		-- Get the current volume for this type
		local current = Scrappy.Audio:getVolume( type )

		-- Do we have one?
		if current then

			-- Create our temp variable for the transition, adjusted to the current volume
			self._transitionVolume = self.getVolume() * current

			-- Create a transition for the volume change, again adjusted to the current volume
			self._volumeTransition = to( self, { time = time, _transitionVolume = level * current, onComplete = onComplete } )

		end

	end

	--- Gets a random track index.
	-- @return The index.
	function self.random()

		-- The index to return
		local index

		-- Loop until we have a random index that isn't the current one
		while ( not index or index == self._current ) and #self._sounds > 1 do

		  	-- Get a random index using Scrappy.Random if it exists, otherwise just regular random
		  	index = Scrappy.Random and Scrappy.Random:inRange( 1, #self._sounds ) or random( 1, #self._sounds )

		end

		-- Return the random index, or the count of sounds if none set i.e. there is only one sound
		return index or #self._sounds

	end

	--- Enter frame handler for this playlist.
	-- @param event The event table.
	function self.enterFrame( event )

		-- Do we have a transition volume?
		if self._transitionVolume then

			-- Then set it
			self.setVolume( self._transitionVolume )

		end

	end

	--- Destroys this playlist.
	function self.destroy()

		-- Cancel the current volume change transition
		self:cancelVolumeChange()

		-- Remove the enterFrame listener
		Runtime:removeEventListener( "enterFrame", self )

	end

	--- Cancels the current volume change transition.
	function self.cancelVolumeChange()

		-- Nil out the transition volume
		self._transitionVolume = nil

		-- Do we have a volume transition?
		if self._volumeTransition then

			-- Cancel it
			cancel( self._volumeTransition )

			-- And nil it out
			self._volumeTransition = nil

		end

	end

	-- Return the playlist object
	return self

end

-- Return the class
return Playlist
